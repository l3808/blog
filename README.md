# instruction to run the project

## 1. Switch to branch develop to get the latest changes

Use this command to switch to branch develop: `git checkout develop`
And then pull it from the branch develop using this command: `git pull origin develop`

## 2. Add your own database string to run the project

Add your own Database String in the .env file. You can use the free database from www.elephantsql.com

- create new instance
- Fill the name of the instance (basically is the name of the project)
- Use the Tiny Turtle (Free) Plan
- Fill the tags, is used to separate your instances between projects. This is primarily used in the project listing view for easire navigation and access control
- Select the region and data center (select the nearest region from your country)
- Confirm and then select the create instance button

## 3. Add an environment file to the project

Add a .env file in the api folder (at the top of your api folder, so nest can find it)

- add your own DATABASE_URL in the .env file
- add your own JWT_SECRET in the .env file

Example of file:

    DATABASE_URL=<your url>
    JWT_SECRET=secretcode12345

## 4. Run the application on local development

Install the dependencies application using this command: `npm install`

Run the application using this command: `npm run start:dev`
